/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.clases;
import java.util.Scanner;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Jose Figueroa
 */
public class TestFactura {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner sc =new Scanner (System.in);
        Factura factura = new Factura();
        int opc = 0, numFactura = 0;
        String rfc="", nombreCliente="",domicilio="",descripcion="",fechaVenta="";
        float totalVenta = 0;
        
        do{
            System.out.println("1- Iniciar el objeto");
            System.out.println("2- Cambiar Numero de Factura");
            System.out.println("3- Cambiar Rfc");
            System.out.println("4- Cambiar Nombre del Cliente");
            System.out.println("5- Cambiar Domicilio");
            System.out.println("6- Cambiar Descripcion");
            System.out.println("7- Cambiar Fecha de Venta");
            System.out.println("8- Cambiar Total a Pagar");
            System.out.println("9- Mostrar Informacion: ");
            System.out.println("0- Salir");
            System.out.print("Dame la opcion: ");
            opc = sc.nextInt();
            
            switch(opc){
                case 1:
                    System.out.print("Digite el numero de Factura: ");
                    numFactura = sc.nextInt();
                    System.out.print("Digite el Rfc: ");
                    rfc = sc.next();
                    sc.nextLine();
                    System.out.print("Digite el nombre del  cliente: ");
                    nombreCliente = sc.nextLine();
                    System.out.print("Digite el domicilio: ");
                    domicilio = sc.nextLine();
                    System.out.print("Digite la descripcion: ");
                    descripcion = sc.nextLine();
                    System.out.print("Digite la fecha de venta: ");
                    fechaVenta = sc.nextLine();
                    System.out.print("Digite el total de venta: ");
                    totalVenta = sc.nextFloat();
                    factura.setNumFactura(numFactura);
                    factura.setRfc(rfc);
                    factura.setNombreCliente(nombreCliente);
                    factura.setDomicilio(domicilio);
                    factura.setDescripcion(descripcion);
                    factura.setFechaVenta(fechaVenta);
                    factura.setTotalVenta(totalVenta);
                    break;
                case 2:
                    System.out.print("Digite el numero de Factura: ");
                    numFactura = sc.nextInt();
                    sc.nextLine();  
                    factura.setNumFactura(numFactura);
                    break;
                case 3:
                    System.out.print("Digite el Rfc: ");
                    rfc = sc.next();
                    factura.setRfc(rfc);
                    break;
                case 4:
                    System.out.print("Digite el nombre del  cliente: ");
                    sc.nextLine();
                    nombreCliente = sc.nextLine();
                    factura.setNombreCliente(nombreCliente);
                    break;
                case 5:
                    System.out.print("Digite el domicilio: ");
                    sc.nextLine();
                    domicilio = sc.nextLine();
                    factura.setDomicilio(domicilio);
                    break;
                case 6:
                    System.out.print("Digite la descripcion: ");
                    sc.nextLine();
                    descripcion = sc.nextLine();
                    factura.setDescripcion(descripcion);
                    break;
                case 7:
                    System.out.print("Digite la fecha de venta: ");
                    sc.nextLine();
                    fechaVenta = sc.next();
                    factura.setFechaVenta(fechaVenta);
                    break;
                case 8:
                    System.out.print("Digite el total de venta: ");
                    totalVenta = sc.nextFloat();
                    factura.setTotalVenta(totalVenta);
                    break;
                case 9:
                    factura.imprimirFactura();
                    break;
                case 0:
                    System.out.println("Salio con exito");
                    break;
                default:
                    System.out.println("Opcion invalida");
            }
        }  while (opc!=0);
    }   
}