/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.clases;

/**
 *
 * @author Jose Figueroa
 */
public class Contrato {

    private int numContrato;
    private String fechaContrato;
    private String nombreEmpleado;
    private String domicilio;
    private int tipoContrato;
    private int nivelEstudio;
    private float pagoDia;
    private int diasTrabajados;

    public Contrato() {
        this.numContrato = 0;
        this.fechaContrato = "";
        this.nombreEmpleado = "";
        this.domicilio = "";
        this.tipoContrato = 0;
        this.nivelEstudio = 0;
        this.pagoDia = 0.0f;
        this.diasTrabajados = 0;
    }

    public Contrato(int numContrato, String fechaContrato, String nombreEmpleado, String domicilio, int tipoContrato, int nivelEstudio, float pagoDia, int diasTrabajados) {
        this.numContrato = numContrato;
        this.fechaContrato = fechaContrato;
        this.nombreEmpleado = nombreEmpleado;
        this.domicilio = domicilio;
        this.tipoContrato = tipoContrato;
        this.nivelEstudio = nivelEstudio;
        this.pagoDia = pagoDia;
        this.diasTrabajados = diasTrabajados;
    }

    public Contrato(Contrato x) {
        this.numContrato = x.numContrato;
        this.fechaContrato = x.fechaContrato;
        this.nombreEmpleado = x.nombreEmpleado;
        this.domicilio = x.domicilio;
        this.tipoContrato = x.tipoContrato;
        this.nivelEstudio = x.nivelEstudio;
        this.pagoDia = x.pagoDia;
        this.diasTrabajados = x.diasTrabajados;
    }

    public int getNumContrato() {
        return numContrato;
    }

    public void setNumContrato(int numContrato) {
        this.numContrato = numContrato;
    }

    public String getFechaContrato() {
        return fechaContrato;
    }

    public void setFechaContrato(String fechaContrato) {
        this.fechaContrato = fechaContrato;
    }

    public String getNombreEmpleado() {
        return nombreEmpleado;
    }

    public void setNombreEmpleado(String nombreEmpleado) {
        this.nombreEmpleado = nombreEmpleado;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public int getTipoContrato() {
        return tipoContrato;
    }

    public void setTipoContrato(int tipoContrato) {
        this.tipoContrato = tipoContrato;
    }

    public int getNivelEstudio() {
        return nivelEstudio;
    }

    public void setNivelEstudio(int nivelEstudio) {
        this.nivelEstudio = nivelEstudio;
    }

    public float getPagoDia() {
        return pagoDia;
    }

    public void setPagoDia(float pagoDia) {
        this.pagoDia = pagoDia;
    }

    public int getDiasTrabajados() {
        return diasTrabajados;
    }

    public void setDiasTrabajados(int diasTrabajados) {
        this.diasTrabajados = diasTrabajados;
    }
    
    public float calcularSubtotal() {
        float incrementoPorNivel = 0.0f;
        float subTotal = 0.0f;
        switch (nivelEstudio) {
            case 1:
                incrementoPorNivel = pagoDia * 0.20f;
                break;
            case 2:
                incrementoPorNivel = pagoDia * 0.50f; //
                break;
            case 3:
                incrementoPorNivel = pagoDia;
                break;
        }
        subTotal = pagoDia * diasTrabajados + incrementoPorNivel * diasTrabajados;
        return subTotal;
    }
    public float calcularImpuesto() {
        float impuesto = 0.0f;
        impuesto = this.calcularSubtotal() * 0.16f;
        return impuesto;
    }
    public float calcularTotalPagar() {
        float totalPagar = 0.0f;
        totalPagar = this.calcularSubtotal() - this.calcularImpuesto();
        return totalPagar;
    }
    public void imprimirContrato() {
        System.out.println("Numero del contrato: " + this.numContrato);
        System.out.println("Fecha: " + this.fechaContrato);
        System.out.println("Nombre del empleado: " + this.nombreEmpleado);
        System.out.println("Domicilio: " + this.domicilio);
        System.out.println("Tipo de contrato: " + this.tipoContrato);
        System.out.println("Nivel de estudio: " + this.nivelEstudio);
        System.out.println("Pago diario: " + this. pagoDia);
        System.out.println("Dias trabajados: " + this.diasTrabajados);
        System.out.println("El subtotal es: " + this.calcularSubtotal());
        System.out.println("El impuesto es: " + this.calcularImpuesto());
        System.out.println("El total pagar es: " + this.calcularTotalPagar());
    }
}