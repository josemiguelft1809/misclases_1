/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.clases;
import java.util.Scanner;
/**
 *
 * @author Jose Figueroa
 */
public class TestContrato {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
Contrato contrato = new Contrato();
        Scanner sc = new Scanner(System.in);
        int opcion, numContrato, tipoContrato, nivelEstudio, diasTrabajados;
        float pagoDia;
        String fechaContrato, nombreEmpleado, domicilio;

        do {
            System.out.println("1. Iniciar objeto");
            System.out.println("2. Cambiar numero de contrato");
            System.out.println("3. Cambiar fecha de contrato");
            System.out.println("4. Cambiar nombre del empleado");
            System.out.println("5. Cambiar domicilio");
            System.out.println("6. Cambiar tipo de contrato");
            System.out.println("7. Cambiar nivel de estudio");
            System.out.println("8. Cambiar pago por dia");
            System.out.println("9. Cambiar dias trabajados");
            System.out.println("10. Imprimir");
            System.out.println("0. Salir");
            System.out.print("Seleccione una opcion: ");
            opcion = sc.nextInt();

            switch (opcion) {
                case 1:
                    System.out.print("Ingrese el numero de contrato: ");
                    numContrato = sc.nextInt();
                    sc.nextLine();
                    System.out.print("Ingrese fecha de contrato: ");
                    fechaContrato = sc.nextLine();
                    System.out.print("Ingrese nombre del empleado: ");
                    nombreEmpleado = sc.nextLine();
                    System.out.print("Ingrese domicilio: ");
                    domicilio = sc.nextLine();
                    System.out.print("Ingrese tipo de contrato: ");
                    tipoContrato = sc.nextInt();
                    System.out.print("Ingrese nivel de estudio: ");
                    nivelEstudio = sc.nextInt();
                    System.out.print("Ingrese pago por dia: ");
                    pagoDia = sc.nextFloat();
                    System.out.print("Ingrese dias trabajados: ");
                    diasTrabajados = sc.nextInt();

                    contrato.setNumContrato(numContrato);
                    contrato.setFechaContrato(fechaContrato);
                    contrato.setNombreEmpleado(nombreEmpleado);
                    contrato.setDomicilio(domicilio);
                    contrato.setTipoContrato(tipoContrato);
                    contrato.setNivelEstudio(nivelEstudio);
                    contrato.setPagoDia(pagoDia);
                    contrato.setDiasTrabajados(diasTrabajados);
                    break;
                case 2:
                    System.out.print("Ingrese el nuevo numero de contrato: ");
                    numContrato = sc.nextInt();
                    contrato.setNumContrato(numContrato);
                    break;
                case 3:
                    System.out.print("Ingrese nueva fecha de contrato: ");
                    sc.nextLine();
                    fechaContrato = sc.nextLine();
                    contrato.setFechaContrato(fechaContrato);
                    break;
                case 4:
                    System.out.print("Ingrese nuevo nombre del empleado: ");
                    sc.nextLine();
                    nombreEmpleado = sc.nextLine();
                    contrato.setNombreEmpleado(nombreEmpleado);
                    break;
                case 5:
                    System.out.print("Ingrese nuevo domicilio: ");
                    sc.nextLine();
                    domicilio = sc.nextLine();
                    contrato.setDomicilio(domicilio);
                    break;
                case 6:
                    System.out.print("Ingrese nuevo tipo de contrato: ");
                    tipoContrato = sc.nextInt();
                    contrato.setTipoContrato(tipoContrato);
                    break;
                case 7:
                    System.out.print("Ingrese nuevo nivel de estudio: ");
                    nivelEstudio = sc.nextInt();
                    contrato.setNivelEstudio(nivelEstudio);
                    break;
                case 8:
                    System.out.print("Ingrese nuevo pago por dia: ");
                    pagoDia = sc.nextFloat();
                    contrato.setPagoDia(pagoDia);
                    break;
                case 9:
                    System.out.print("Ingrese nuevos dias trabajados: ");
                    diasTrabajados = sc.nextInt();
                    contrato.setDiasTrabajados(diasTrabajados);
                    break;
                case 10:
                    contrato.imprimirContrato();
                    break;
                case 0:
                    System.out.println("Salio exitosamente");
                    break;
                default:
                    System.out.println("Opcion invalida");
            }
        } while (opcion != 0);
    }
}
