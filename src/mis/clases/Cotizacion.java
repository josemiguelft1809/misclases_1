/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.clases;

/**
 *
 * @author Jose Figueroa
 */
public class Cotizacion {
    private int numCotizacion;
    private String descripcionAutomovil;
    private float precio;
    private float porcentajePagoInicial;
    private int plazo;
    
    public Cotizacion(){
        this.numCotizacion=0;
        this.descripcionAutomovil=null;
        this.precio=0;
        this.porcentajePagoInicial=0;
        this.plazo=0;  
    }
    
    public Cotizacion(int numCotizacion, String descripcionAutomovil, float precio, float porcentajePagoInicial, int plazo){
        this.numCotizacion = numCotizacion;
        this.descripcionAutomovil = descripcionAutomovil;
        this.precio = precio;
        this.porcentajePagoInicial = porcentajePagoInicial;
        this.plazo = plazo; 

    }
    
    public Cotizacion(Cotizacion x){
        this.numCotizacion = x.numCotizacion;
        this.descripcionAutomovil = x.descripcionAutomovil;
        this.precio = x.precio;
        this.porcentajePagoInicial = x.porcentajePagoInicial;
        this.plazo = x.plazo; 
    }

    public int getNumCotizacion() {
        return numCotizacion;
    }

    public void setNumCotizacion(int numCotizacion) {
        this.numCotizacion = numCotizacion;
    }

    public String getDescripcionAutomovil() {
        return descripcionAutomovil;
    }

    public void setDescripcionAutomovil(String descripcionAutomovil) {
        this.descripcionAutomovil = descripcionAutomovil;
    }

    public float getPrecio() {
        return precio;
    }

    public void setPrecio(float precio) {
        this.precio = precio;
    }

    public float getPorcentajePagoInicial() {
        return porcentajePagoInicial;
    }

    public void setPorcentajePagoInicial(float porcentajePagoInicial) {
        this.porcentajePagoInicial = porcentajePagoInicial;
    }

    public int getPlazo() {
        return plazo;
    }

    public void setPlazo(int plazo) {
        this.plazo = plazo;
    }
    
    //metodos de comportamiento
    public float calcularPagoInicial(){
        float pagoInicial = 0;
        pagoInicial = (float) (this.precio * (this.porcentajePagoInicial/100));
        return pagoInicial;
    }
    
    public float calcularTotalFinanciar(){
        float totalFinanciar = 0;
        totalFinanciar = this.precio - this.calcularPagoInicial();
        return totalFinanciar;
    }
    
    public float calcularPagoMensual(){
        float pagoMensual = 0;
        pagoMensual = this.calcularTotalFinanciar() /this.plazo;
        return pagoMensual;
    }
    
    void imprimirCotizacion(){
        System.out.println("Numero de Cotizacion: " + this.numCotizacion);
        System.out.println("Descripcion: " + this.descripcionAutomovil);
        System.out.println("Precio: $" + this.precio);
        System.out.println("Porcentaje del pago Inicial: " + this.porcentajePagoInicial);
        System.out.println("Plazo en meses: " + this.plazo);
        System.out.println("Pago Inicial: $" + this.calcularPagoInicial());
        System.out.println("Total a Financiar: $" + this.calcularTotalFinanciar());
        System.out.println("Pago Mensual: $" + this.calcularPagoMensual());
    }
}