/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mis.clases;

/**
 *
 * @author Jose Figueroa
 */
public class Factura {
    private int numFactura;
    private String rfc;
    private String nombreCliente;
    private String domicilio;
    private String descripcion;
    private String fechaVenta;
    private float totalVenta;

    public Factura() {
        this.numFactura = 0;
        this.rfc= "" ;
        this.nombreCliente = "";
        this.domicilio="";
        this.descripcion="";
        this.fechaVenta="";
        this.totalVenta=0;
    }

    public Factura(int numFactura, String rfc, String nombreCliente, String domicilio, String descripcion, String fechaVenta, float totalVenta) {
        this.numFactura = numFactura;
        this.rfc = rfc;
        this.nombreCliente = nombreCliente;
        this.domicilio = domicilio;
        this.descripcion = descripcion;
        this.fechaVenta = fechaVenta;
        this.totalVenta = totalVenta;
    }
    
    public Factura(Factura x){
        this.numFactura=x.numFactura;
        this.rfc=x.rfc;
        this.nombreCliente=x.nombreCliente;
        this.domicilio=x.domicilio;
        this.descripcion=x.descripcion;
        this.fechaVenta=x.fechaVenta;
        this.totalVenta=x.totalVenta;
    }

    public int getNumFactura() {
        return numFactura;
    }

    public void setNumFactura(int numFactura) {
        this.numFactura = numFactura;
    }

    public String getRfc() {
        return rfc;
    }

    public void setRfc(String rfc) {
        this.rfc = rfc;
    }

    public String getNombreCliente() {
        return nombreCliente;
    }

    public void setNombreCliente(String nombreCliente) {
        this.nombreCliente = nombreCliente;
    }

    public String getDomicilio() {
        return domicilio;
    }

    public void setDomicilio(String domicilio) {
        this.domicilio = domicilio;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFechaVenta() {
        return fechaVenta;
    }

    public void setFechaVenta(String fechaVenta) {
        this.fechaVenta = fechaVenta;
    }

    public float getTotalVenta() {
        return totalVenta;
    }

    public void setTotalVenta(float totalVenta) {
        this.totalVenta = totalVenta;
    }
    
    public float calcularImpuesto(){
        float calcularImpuesto = 0;
        calcularImpuesto = (float) (this.totalVenta *0.16);
        return calcularImpuesto;
    }
    public float calcularTotalPagar(){
        float calcularTotalPagar = 0;
        calcularTotalPagar = this.totalVenta +this.calcularImpuesto();
        return calcularTotalPagar;
    }
    void imprimirFactura(){
        System.out.println("Numero de Factura: "+ this.numFactura);
        System.out.println("Rfc: "+ this.rfc);
        System.out.println("Nombre del Cliente: "+ this.nombreCliente);
        System.out.println("Domicilio: "+ this.domicilio);
        System.out.println("Descripcion: "+ this.descripcion);
        System.out.println("Fecha de Venta: "+ this.fechaVenta);
        System.out.println("Total de Venta: $"+ this.totalVenta);
        System.out.println("Impuesto: $"+ this.calcularImpuesto());
        System.out.println("Total a Pagar: $"+ this.calcularTotalPagar());
    }
}